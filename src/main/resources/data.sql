create table IF NOT EXISTS tbl_games
(
    game_id    bigserial
        primary key,
    time_created timestamp,
    state varchar(255),
    title varchar(255),
    radius bigint,
    latitude bigint,
    longitude bigint,
    player_limit bigint
);

create table IF NOT EXISTS tbl_players
(
    player_id       bigserial
        primary key,
    player_name     varchar(255),
    bite_code       varchar(255),
    is_human        boolean,
    is_patient_zero boolean,
    game_id         bigint
        constraint fk_games
            references tbl_games
);

create table IF NOT EXISTS tbl_chats
(
    chat_id   bigserial
        primary key,
    message   varchar(255),
    channel   varchar(255),
    faction   varchar(255),
    player_name    varchar(255),
    game_id   bigint
        constraint fk_games
            references tbl_games
);

create table IF NOT EXISTS tbl_kills
(
    kill_id       bigserial
        primary key,
    time_of_death timestamp,
    game_id       bigint
        constraint fk_games
            references tbl_games,
    killer_id     bigint
        constraint fk_killer
               references tbl_players,
    victim_id     bigint,
    bite_code       varchar(255),
    latitude    bigint,
    longitude   bigint
        constraint fk_victim
            references tbl_players
);

INSERT INTO tbl_games (state, time_created, title, radius, latitude, longitude, player_limit) VALUES ('Registration', '3/28/20, 9:05 AM','Slottet', 300, 55.604629, 12.987721, 5);
INSERT INTO tbl_games (state, time_created, title, radius, latitude, longitude, player_limit) VALUES ('In Progress', '3/20/20, 9:05 AM', 'Kontoret', 100, 55.604246, 13.001690, 50);
INSERT INTO tbl_games (state, time_created, title, radius, latitude, longitude, player_limit) VALUES ('Completed','3/23/20, 9:05 AM',  'Arlöv', 3000, 55.640006, 13.088700, 100);
INSERT INTO tbl_games (state, time_created, title, radius, latitude, longitude, player_limit) VALUES ('Registration', '3/16/20, 9:05 AM', 'Möllan', 1000, 55.591621, 13.007847, 50);
INSERT INTO tbl_games (state, time_created, title, radius, latitude, longitude, player_limit) VALUES ('In Progress', '3/1/20, 9:05 AM', 'Demo', 100, 55.604246, 13.001690, 5);

INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Pickleniels', 'GRIMAS', true, false, 1);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('SniperBasti', 'ABCDEF' , true, false, 1);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('ChrisTheSlayer', 'BADASS', true, false, 1);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('AssassinAlex', 'SOCKET', true, false, 1);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Herman', 'GALANT', true, false, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Olle', 'GARANT', false, false, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Anita', 'HEMSAM', false, false, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Britt', 'DAGBOK', true, false, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Bengt', 'JUNIOR', true, false, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Harmlesshuman', 'FARLIG' , false, true, 2);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Hjördis', 'HUNDAR' , false, false, 3);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Olga', 'HALLOJ' , true, false, 3);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Lisbet', 'LISBET' , false, false, 3);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Gunilla', 'RABIES' , false, true, 3);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('Hilda', 'KAMREM' , false, false, 3);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('alex', 'ALEXON' , true, false, 5);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('christopher', 'CHRIST' , true, false, 5);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('sebastian', 'SEBAST' , false, true, 5);
INSERT INTO tbl_players (player_name, bite_code, is_human, is_patient_zero, game_id) VALUES ('nils', 'NILJAC' , true, false, 5);

INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('hello', 'global', 'human', 'Pickleniels', 1);
INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('hi', 'global', 'zombie', 'Zombie-Nils', 1);
INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('i just saw a zombie', 'faction', 'human', 'ChrisTheSlayer', 1);
INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('why are you hey', 'faction', 'zombie', 'Zombie-Nils', 1);
INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('Please I want to live', 'global', 'human', 'Herman', 2);
INSERT INTO tbl_chats (message, channel, faction, player_name, game_id) VALUES ('I am definitely not patient zero', 'global', 'zombie', 'Harmlesshuman', 2);

INSERT INTO tbl_kills (game_id, killer_id, victim_id, latitude, longitude, bite_code, time_of_death) VALUES (2, 10, 6, 55.6042464, 13.0016902,'GARANT', '2022-03-25T08:35:26.685Z');
INSERT INTO tbl_kills (game_id, killer_id, victim_id, latitude, longitude, bite_code, time_of_death) VALUES (2, 10, 7, 55.6048461, 13.0017909,'HEMSAM', '2022-03-25T08:40:26.685Z');
