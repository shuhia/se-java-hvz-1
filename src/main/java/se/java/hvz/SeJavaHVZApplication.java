package se.java.hvz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeJavaHVZApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeJavaHVZApplication.class, args);
    }
}
