package se.java.hvz.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.java.hvz.Models.Kill;
import se.java.hvz.Models.Player;

import java.util.List;

@Repository
public interface KillRepository extends JpaRepository<Kill, Long> {
    List<Kill> findAllByGameGameId(Long game_id);
    Kill findKillByGameGameIdAndKillId(Long game_id, Long kill_id);
    Kill findKillByKillId(Long kill_id);
    void deleteKillByGameGameIdAndKillId(Long game_id, Long kill_id);
    void deleteKillByGameGameId(Long game_id);
    void deleteKillByVictimOrKiller(Player victim, Player killer);
    void deleteAllByGameGameId(Long game_id);
}
