package se.java.hvz.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.java.hvz.Models.Game;
import se.java.hvz.Models.Player;
import se.java.hvz.Models.dto.PlayerDto;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    List<Player> findAllByGameGameId(Long game_id);
    List<PlayerDto> findByGameGameId(Long game_id);
    Player findPlayerByGameGameIdAndPlayerId(Long game_id, Long player_id);
    Player findPlayerByBiteCode(String bite_code);
    Player findPlayerByGameGameIdAndPlayerName(Long game_id, String player_name);
    Player findPlayerByGameGameIdAndIsPatientZeroIsTrue(Long game_id);
    Player findPlayerByPlayerNameAndGameGameId(String player_name, Long game_id);
    Player findPlayerByPlayerId(Long player_id);
    List<Player> findAllByGameGameIdAndIsHumanIsTrue(Long game_id);
    void deletePlayerByGameGameIdAndPlayerId(Long game_id, Long player_id);
}
