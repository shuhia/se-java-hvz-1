package se.java.hvz.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.java.hvz.Models.Game;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
