package se.java.hvz.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.java.hvz.Models.Chat;
import se.java.hvz.Models.Game;
import se.java.hvz.Models.Player;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
    List<Chat> findAllByGameGameId(Long game_id);
    void deleteAllByGame(Game game);
}
