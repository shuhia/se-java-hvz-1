package se.java.hvz.Models.dto;

public class PlayerDto {
    private Long playerId;
    private String playerName;

    public PlayerDto(Long playerId, String playerName) {
        this.playerId = playerId;
        this.playerName = playerName;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
