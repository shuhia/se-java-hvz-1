package se.java.hvz.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@EntityListeners(GameEventListener.class)
@Entity
@Table(name = "tbl_games")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gameId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCreated;

    private String title;

    private String state;

    @Size(max = 3000)
    private Long radius;

    @Column(precision = 3, scale = 15)
    private double latitude;

    @Column(precision = 3, scale = 15)
    private double longitude;

    @JsonGetter("playerCount")
    public int players() {
        if (players==null) return 0;
        else return players.size();
    }

    @JsonSetter("playerCount")
    public void setPlayerCount(int playerCount) {}

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    private List<Player> players;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Kill> kills;

    @Size(max = 100)
    private Long playerLimit;

    public Game() {
    }

    public Game(Long gameId) {
        this.gameId = gameId;
    }

    public Game(Long gameId, String title, String state, Long radius, double latitude, double longitude, List<Player> players, Long playerLimit) {
        this.gameId = gameId;
        this.title = title;
        this.state = state;
        this.radius = radius;
        this.latitude = latitude;
        this.longitude = longitude;
        this.players = players;
        this.playerLimit = playerLimit;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Long getGameId() {
        return gameId;
    }

    public String getTitle() {
        return title;
    }

    public Long getRadius() {
        return radius;
    }

    public void setRadius(Long radius) {
        this.radius = radius;
    }

    public String getState() {
        return state;
    }

    public void setGameId(Long id) {
        this.gameId = id;
    }

    public Long getPlayerLimit() {
        return playerLimit;
    }

    public void setPlayerLimit(Long playerLimit) {
        this.playerLimit = playerLimit;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setState(String state) {
        this.state = state;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<Kill> getKills() {
        return kills;
    }

    public void setKills(List<Kill> kills) {
        this.kills = kills;
    }

}
