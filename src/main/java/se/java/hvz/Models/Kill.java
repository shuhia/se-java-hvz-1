package se.java.hvz.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@EntityListeners(KillEventListener.class)
@Entity
@Table(name = "tbl_kills")
public class Kill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long killId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeOfDeath;

    @Column(precision = 3, scale = 15)
    private double latitude;

    @Column(precision = 3, scale = 15)
    private double longitude;

    @JsonGetter("killerId")
    public Long killer() {
        return getKiller().getPlayerId();
    }

    @JsonGetter("killerName")
    public String getKillerName(){
        return this.killer.getPlayerName();
    }

    @JsonSetter("killerName")
    public void setKillerName(String killerName){}

    @OneToOne
    @JoinColumn(name = "killer_id")
    private Player killer;

    @JsonGetter("victimId")
    public Long victim() {
        return getVictim().getPlayerId();
    }

    @JsonGetter("victimName")
    public String getVictimName(){
        return this.victim.getPlayerName();
    }

    @JsonSetter("victimName")
    public void setVictimName(String victimName){}


    @OneToOne
    @JoinColumn(name = "victim_id")
    private Player victim;

    @JsonGetter("gameId")
    public Long game() {
        return getGame().getGameId();
    }

    @JsonSetter("gameId")
    public void setTheGame(String gameId) {}

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    private String biteCode;

    public Kill() {
    }

    public Kill(Game game) {
        this.game = game;
    }

    public Kill(Long killId, Date timeOfDeath, double latitude, double longitude, Player killer, Player victim, Game game) {
        this.killId = killId;
        this.timeOfDeath = timeOfDeath;
        this.latitude = latitude;
        this.longitude = longitude;
        this.killer = killer;
        this.victim = victim;
        this.game = game;
    }

    public Long getKillId() {
        return killId;
    }

    public void setKillId(Long killId) {
        this.killId = killId;
    }

    public Date getTimeOfDeath() {
        return timeOfDeath;
    }

    public void setTimeOfDeath(Date timeOfDeath) {
        this.timeOfDeath = timeOfDeath;
    }

    public Player getKiller() {
        return killer;
    }

    public void setKiller(Player killer) {
        this.killer = killer;
    }

    public Player getVictim() {
        return victim;
    }

    public void setVictim(Player victim) {
        this.victim = victim;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public void setBiteCode(String biteCode) {
        this.biteCode = biteCode;
    }
}
