package se.java.hvz.Models;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "tbl_chats")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long chatId;

    private String message;

    private String channel;

    private String faction;

    private String playerName;

    @JsonGetter("gameId")
    public Long game() {
        return getGame().getGameId();
    }

    @OneToOne
    @JoinColumn(name = "game_id")
    private Game game;

    public Chat() {
    }

    public Chat(Long chatId, String message, String channel, String faction, String playerName, Game game) {
        this.chatId = chatId;
        this.message = message;
        this.channel = channel;
        this.faction = faction;
        this.playerName = playerName;
        this.game = game;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
