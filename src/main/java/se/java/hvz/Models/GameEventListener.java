package se.java.hvz.Models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.PostUpdate;

@Component
public class GameEventListener {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @PostUpdate
    private void afterAnyUpdate(Game game) {
        String game_id = String.valueOf(game.getGameId());
        this.simpMessagingTemplate.convertAndSend("/topic/game/"+game_id, game);
    }
}
