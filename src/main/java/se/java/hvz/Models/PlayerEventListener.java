package se.java.hvz.Models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

@Component
public class PlayerEventListener {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @PostPersist
    @PostUpdate
    public void afterAnyUpdate(Player player) {
        String game_id = String.valueOf(player.getGame().getGameId());
        String player_id = String.valueOf(player.getPlayerId());
        this.simpMessagingTemplate.convertAndSend("/topic/game/"+game_id+"/player/"+player_id, player);
    }
}
