package se.java.hvz.Controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.java.hvz.Models.Chat;
import se.java.hvz.Models.CommonResponse;
import se.java.hvz.Models.Game;
import se.java.hvz.Models.Player;
import se.java.hvz.Repositories.ChatRepository;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import se.java.hvz.Repositories.GameRepository;
import se.java.hvz.Repositories.PlayerRepository;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/game")
@SecurityRequirement(name = "keycloak_implicit")
public class ChatController {

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    // GET /game/<game_id>/chat
    // Returns a list of chat messages for a specific game.
    @GetMapping("/{game_id}/chat")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<List<Chat>>> getAllChatMessagesByGameId(@PathVariable Long game_id) {
        try {
            return ResponseEntity
                    .ok(new CommonResponse<>(chatRepository.findAllByGameGameId(game_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // POST /game/<game_id>/chat
    // Send a new chat message.
    @PostMapping("/{game_id}/chat")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Chat>> sendChatMessageByGameId(@PathVariable Long game_id,  @RequestBody Chat chat) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if(game.getState().equals("Completed")){
                return ResponseEntity
                        .ok(new CommonResponse<>(400,"The game is completed. No further actions can be taken."));
            }
            chat.setGame(game);

            return ResponseEntity
                .ok(new CommonResponse<>(chatRepository.save(chat)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    @MessageMapping("/global")
    @SendTo("/topic/global")
    public Chat onGlobal(Chat message) throws Exception {
        return message;
    }

    @MessageMapping("/game")
    public void broadcastNews(String message) throws Exception {
        this.simpMessagingTemplate.convertAndSend("/topic/game", message);
    }



}
