package se.java.hvz.Controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.java.hvz.Models.CommonResponse;
import se.java.hvz.Models.Game;
import se.java.hvz.Models.Kill;
import se.java.hvz.Models.Player;
import se.java.hvz.Repositories.ChatRepository;
import se.java.hvz.Repositories.GameRepository;
import se.java.hvz.Repositories.KillRepository;
import se.java.hvz.Repositories.PlayerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/game")
@SecurityRequirement(name = "keycloak_implicit")
public class GameController {

    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ChatRepository chatRepository;
    @Autowired
    private KillRepository killRepository;

    // Returns a list of games. Accessible by all. In file: src/main/java/se/java/hvz/Configs/SecurityConfig.java Has exception on line 45: ".antMatchers(HttpMethod.GET, "/api/v1/game").permitAll()"
    @GetMapping
    public ResponseEntity<CommonResponse<List<Game>>> getAllGames() {
        return ResponseEntity
                .ok(new CommonResponse<>(gameRepository.findAll()));
    }

    // Returns a specific game object. Accessible by logged-in users without admin privileges.
    @GetMapping("/{game_id}")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Game>> getGameById(@PathVariable Long game_id) {
        try {
            return ResponseEntity
                    .ok(new CommonResponse<>(gameRepository.findById(game_id).get()));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // Creates a new game. Admin only.
    @PostMapping
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<CommonResponse<Game>> createGame(@RequestBody Game game) {
        //Returns error message if admin attempts to create a game with a Game.state other than "Registration".
        if (!game.getState().equals("Registration")) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "Invalid game state. Must be set to Registration."));
        }
        return ResponseEntity
                .ok(new CommonResponse<>(gameRepository.save(game)));
    }

    // Updates a game. Admin only.
    @PutMapping("/{game_id}")
    @PreAuthorize("hasRole('admin')")
    @Transactional
    public ResponseEntity<CommonResponse<Game>> updateGameById(@PathVariable Long game_id, @RequestBody Game gameUpdate) {
        try {
            Game game = gameRepository.findById(game_id).get();
            //Returns error message if admin attempts to update a game that has already been completed.
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            if (!gameUpdate.getState().equals("Registration") && !gameUpdate.getState().equals("In Progress") && !gameUpdate.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "Invalid game state."));
            }

            if (gameUpdate.getState().equals("In Progress") && game.players() <= 3) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "Could not start game. Minimum players required is three."));
            }

            //Only these values from RequestBody will be updated
            game.setState(gameUpdate.getState());
            game.setTitle(gameUpdate.getTitle());

            //When the game is started (Game state sets to "In Progress") a random player becomes patient zero.
            List<Player> players = game.getPlayers();
            if (game.getState().equals("In Progress") && playerRepository.findPlayerByGameGameIdAndIsPatientZeroIsTrue(game_id) == null) {
                int random = (int) (Math.random() * players.size());
                Player playerToBePatientZero = players.get(random);
                playerToBePatientZero.setPatientZero(true);
                playerToBePatientZero.setHuman(false);
                playerRepository.save(playerToBePatientZero);
            }

            if (gameUpdate.getState().equals("Registration")) {
                killRepository.deleteAllByGameGameId(game_id);
                for (Player player : players) {
                    player.setHuman(true);
                    player.setPatientZero(false);
                    playerRepository.save(player);
                }
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(gameRepository.save(game)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // DELETE /game/<game_id>
    // Deletes a game. Admin only.
    @DeleteMapping("/{game_id}")
    @PreAuthorize("hasRole('admin')")
    @Transactional
    public ResponseEntity<CommonResponse<Game>> deleteGameById(@PathVariable Long game_id) {
        try {
            Game game = gameRepository.findById(game_id).get();
            //Must manually delete chats related to the game first because removePlayerFromGameByPlayerId doesn't cascade to delete chat.
            chatRepository.deleteAllByGame(game);
            gameRepository.deleteById(game_id);
            return ResponseEntity
                    .ok(new CommonResponse<>(game));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }
}
