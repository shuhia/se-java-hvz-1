package se.java.hvz.Controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.java.hvz.Models.CommonResponse;
import se.java.hvz.Models.Game;
import se.java.hvz.Models.Kill;
import se.java.hvz.Models.Player;
import se.java.hvz.Repositories.GameRepository;
import se.java.hvz.Repositories.KillRepository;
import se.java.hvz.Repositories.PlayerRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/game")
@SecurityRequirement(name = "keycloak_implicit")
public class KillController {

    @Autowired
    KillRepository killRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    GameRepository gameRepository;

    // GET /game/<game_id>/kill
    // Get a list of kills.
    @GetMapping("/{game_id}/kill")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<List<Kill>>> getAllKillsByGameId(@PathVariable Long game_id) {
        try{
            return ResponseEntity
                .ok(new CommonResponse<>(killRepository.findAllByGameGameId(game_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // GET /game/<game_id>/kill/<kill_id>
    // Returns a specific kill object.
    @GetMapping("/{game_id}/kill/{kill_id}")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Kill>> getKillByGameIdAndKillId(@PathVariable Long game_id, @PathVariable Long kill_id) {
        try {
            Game game = gameRepository.findById(game_id).get();
            Kill kill = killRepository.findKillByKillId(kill_id);
            if (kill == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No kill found."));
            }
            if(!kill.getGame().getGameId().equals(game.getGameId())){
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No kill found."));
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(killRepository.findKillByGameGameIdAndKillId(game_id, kill_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // POST /game/<game_id>/kill
    // Creates a kill object by looking up the victim by the specified bite code.
    @PostMapping("/{game_id}/kill")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Kill>> killPlayerByGameId(@PathVariable Long game_id, @RequestBody Kill kill) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            if (game.getState().equals("Registration")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is not in progress. Kills are not allowed."));
            }

            Player victim = playerRepository.findPlayerByBiteCode(kill.getBiteCode());
            if (playerRepository.findPlayerByBiteCode(kill.getBiteCode()) == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The bite code does not match any player."));
            }

            Player killer = playerRepository.findPlayerByPlayerId(kill.getKiller().getPlayerId());
            if (killer == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "Killer not found."));
            }
            kill.setGame(game);
            kill.setBiteCode(victim.getBiteCode());

            if (!victim.getGame().getGameId().equals(killer.getGame().getGameId())) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The players are not in the same game."));
            } else if (killer.getHuman()) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The killer has to be a zombie."));
            } else if (!victim.getHuman()) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The victim has to be a human."));
            } else {
                kill.setKiller(killer);
                kill.setVictim(victim);
                victim.setHuman(false);
                playerRepository.save(victim);
            }

            killRepository.save(kill);

            if(playerRepository.findAllByGameGameIdAndIsHumanIsTrue(game_id).size() == 1){
                game.setState("Completed");
                gameRepository.save(game);
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(kill));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // PUT /game/<game_id>/kill/<kill_id>
    // Updates a kill object.
    @PutMapping("/{game_id}/kill/{kill_id}")
    @PreAuthorize("hasRole('user')")
    @Transactional
    public ResponseEntity<CommonResponse<Kill>> updateKillByKillIdAndGameId(@PathVariable Long game_id, @PathVariable Long kill_id, @RequestBody Kill killUpdate) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            Kill kill = killRepository.findKillByGameGameIdAndKillId(game_id, kill_id);
            if (kill == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No kill found."));
            }
            kill.setLatitude(killUpdate.getLatitude());
            kill.setLongitude(killUpdate.getLongitude());
            if (game.getState().equals("Registration")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is not in progress. Kills are not allowed."));
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(killRepository.save(kill)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // DELETE /game/<game_id>/kill/<kill_id>
    // Delete a kill. Admin only.
    @DeleteMapping("/{game_id}/kill/{kill_id}")
    @PreAuthorize("hasRole('admin')")
    @Transactional
    public ResponseEntity<CommonResponse<Kill>> deleteKillByKillIdAndGameId(@PathVariable Long game_id, @PathVariable Long kill_id) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            Kill deletedKill = killRepository.findKillByGameGameIdAndKillId(game_id, kill_id);
            if (deletedKill == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No kill found."));
            }
            killRepository.deleteKillByGameGameIdAndKillId(game_id, kill_id);

            return ResponseEntity
                    .ok(new CommonResponse<>(deletedKill));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }
}
